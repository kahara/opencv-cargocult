FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04

ENV \
    PYTHON_VERSIONS="3.8.1 3.7.6 3.6.10" \
    NUMPY_VERSION="1.18.4" \
    OPENCV_VERSION="4.3.0" \
    CUDA_ARCH_BIN="7.5"

RUN apt update \
    && apt install -y \
        build-essential \
        cmake \
        curl \
        git \
        libavcodec-dev \
        libavformat-dev \
        libdc1394-22-dev \
        libffi-dev \
        libjpeg-dev \
        libssl-dev \
        libpng-dev \
        libtiff-dev \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        pkg-config

#        libgtk2.0-dev \
#        libjasper1 \
#        libjasper1-dev \

RUN curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash \
    && for pyver in $PYTHON_VERSIONS; do /root/.pyenv/plugins/python-build/bin/python-build $pyver /usr/local/; done

# Install Cython, Numpy
RUN for pyver in $PYTHON_VERSIONS; do pymajorminor="python$(echo $pyver | rev | cut -d. -f2- | rev)" \
    && $pymajorminor -m pip install --upgrade pip \
    && $pymajorminor -m pip install Cython --install-option="--no-cython-compile" \
    && $pymajorminor -m pip install numpy==$NUMPY_VERSION; \
    done

# Get OpenCV & contrib sources
COPY . /build
WORKDIR /build
RUN opencv_source="opencv-${OPENCV_VERSION}.tar.gz" && opencv_contrib_source="opencv_contrib-${OPENCV_VERSION}.tar.gz" \
    && if [ ! -f $opencv_source ]; then \
    curl -L -o $opencv_source "https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.tar.gz"; fi \
    && if [ ! -f $opencv_contrib_source ]; then \
    curl -L -o $opencv_contrib_source "https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.tar.gz"; fi \
    && zcat $opencv_source | tar xf - \
    && zcat $opencv_contrib_source | tar xf -

# Compile OpenCV
RUN opencv_root="${PWD}/opencv-${OPENCV_VERSION}" && opencv_contrib_root="${PWD}/opencv_contrib-${OPENCV_VERSION}" \
    && for pyver in $PYTHON_VERSIONS; do pymajorminor="python$(echo $pyver | rev | cut -d. -f2- | rev)" \
    && cd $opencv_root \
    && mkdir release-$pymajorminor && cd release-$pymajorminor \
    && cmake -DCMAKE_BUILD_TYPE=RELEASE -DOPENCV_EXTRA_MODULES_PATH="${opencv_contrib_root}/modules" \
    -D BUILD_opencv_python3=ON -D BUILD_opencv_python2=OFF \
    -D PYTHON_DEFAULT_EXECUTABLE=/usr/local/bin/$pymajorminor \
    -D PYTHON3_EXECUTABLE=/usr/local/bin/$pymajorminor \
    -D PYTHON_INCLUDE_DIR=$($pymajorminor -c 'import sysconfig; print(sysconfig.get_config_var("INCLUDEPY"))') \
    -D PYTHON_LIBRARY=/usr/local/lib/$($pymajorminor -c 'import sysconfig; print(sysconfig.get_config_var("LIBRARY"))') \
    -D PYTHON3_PACKAGES_PATH=/usr/local/lib/$pymajorminor/site-packages \
    -D WITH_CUDA=ON -D CUDA_ARCH_BIN="${CUDA_ARCH_BIN}" -D CUDA_ARCH_PTX="" \
    -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_EXAMPLES=OFF -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j $(nproc) && make install && cd python_loader && $pymajorminor -m pip install -e . ; \
    done
